package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class EditLeadPage extends ProjectMethods{

	public EditLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.ID, using = "updateLeadForm_companyName")
	WebElement eleCompanyName;
	@And("Enter the new Company Name as (.*)")
	public EditLeadPage enterCompanyName(String data) {	
		type(eleCompanyName, data);
		return this;
	}
	
	@FindBy(how=How.ID,using ="updateLeadForm_firstName")
	WebElement eleFirstName;
	@And("Enter the new first Name as (.*)")
	public EditLeadPage enterFirstName(String data) {
		type(eleFirstName, data);
		return this;		
	}
	
	@FindBy(how=How.ID,using ="updateLeadForm_lastName")
	WebElement eleLastName;
	@And("Enter the new lastName as (.*)")
	public EditLeadPage enterLastName(String data) {
		type(eleLastName, data);
		return this;		
	}
		
	@FindBy(how=How.XPATH,using="//input[@value='Update']")
	WebElement eleUpdate;
	@And("Click on Update Button")
	public ViewLeadPage clickUpdate() {
		click(eleUpdate);
		return new ViewLeadPage();
	}
	
	
	
}
