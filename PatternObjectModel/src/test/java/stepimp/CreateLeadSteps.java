/*package stepimp;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class CreateLeadSteps {
	public RemoteWebDriver driver;
public String fname;
	@Given("Open the Browser")
	public void open_the_Browser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();

	}

	@Given("maximize the Browser")
	public void maximize_the_Browser() {
		driver.manage().window().maximize();

	}

	@Given("Set the TimeOut")
	public void set_the_TimeOut() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	}

	@Given("Launch the URL as (.*)")
	public void launch_the_URL(String url) {
		driver.get(url);

	}

	@Given("Enter the UserName as (.*)")
	public void enter_the_UserName(String userName)  {
		driver.findElementById("username").sendKeys(userName);
		

	}

	@Given("Enter the PassWord as (.*)")
	public void enter_the_PassWord_as_Crmsfa(String password) {
		driver.findElementById("password").sendKeys(password);

	}

	@Given("Click Login Button")
	public void click_Login_Button() {
		driver.findElementByClassName("decorativeSubmit").click();

	}

	@Given("Click Crmsfa")
	public void click_Crmsfa() {
		driver.findElementByLinkText("CRM/SFA").click();
	}

	@Then("Verify the LeadsPage")
	public void verify_the_LeadsPage() {

	}

	@Then("Click on Leads Tab")
	public void click_on_Leads_Tab() {
		driver.findElementByLinkText("Leads").click();

	}

	@Then("Click on CreateLead")
	public void click_on_CreateLead() {

		driver.findElementByLinkText("Create Lead").click();
	}

	@Then("Enter the FirstName as (.*)")
	public void enter_the_FirstName(String firstName) {
		fname=firstName;
		driver.findElementById("createLeadForm_firstName").sendKeys(firstName);

	}

	@Then("Enter the CompanyName as (.*)")
	public void enter_the_CompanyName(String companyName) {
		driver.findElementById("createLeadForm_companyName").sendKeys(companyName);

	}

	@Then("Enter the LastName as (.*)")
	public void enter_the_LastName(String lastName) {
		driver.findElementById("createLeadForm_lastName").sendKeys(lastName);
	}

	@Then("Click on CreateButton")
	public void click_on_CreateButton() {
		driver.findElementByClassName("smallSubmit").click();
	}

	@Then("Verify the Creation of Lead with FirstName")
	public void verify_the_Creation_of_Lead() {
		
		if(fname.equalsIgnoreCase(driver.findElementById("viewLead_firstName_sp").getText())){
			System.out.println("FirstName Verified");	
		}
			
		
	}

	@Then("Close the Browser")
	public void close_the_Browser() {
		driver.close();
	}
}
*/