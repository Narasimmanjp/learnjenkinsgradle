package stepimp;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import wdMethods.SeMethods;

public class Hooks extends SeMethods {

	@Before
	public void runBefore(Scenario sc) {
		
		startResult();
		//startTestModule(testCaseName, testDescription);
		startTestModule(sc.getName(), sc.getId());
		test = startTestCase("Nodes");
		test.assignCategory("Fundtional");
		test.assignAuthor("Narasimman");
		startApp("chrome", "http://leaftaps.com/opentaps");		
	
		/*System.out.println("Test Scenario Name  : " + sc.getName());
		System.out.println("Test Scenario ID  : " + sc.getId());*/
	}

	@After
	public void afterRun(Scenario sc) {
		
		closeAllBrowsers();
		endResult();
		/*System.out.println("Feature file uri  " + sc.getUri());
		System.out.println("Status of the Test  " + sc.getStatus());
		System.out.println("Is Failed  " + sc.isFailed());*/
		
	}

}
